let Bicycle = require('../../models/bicycle');
let mongoose = require('mongoose');

describe('Testing Bicicletas ', () => {
	beforeAll((done) => { mongoose.connection.close(done) });

	beforeEach((done) => {
		mongoose.disconnect();
		let mongoDB = "mongodb://localhost/testdb";
		mongoose.connect(mongoDB, { useNewUrlParser:true, useUnifiedTopology: true  });

		const db = mongoose.connection;
		db.on('error', console.error.bind(console, 'connection error'));
		db.once('open', () => {
			console.log('We are connected to test database');
			done();
		});
	});

	afterEach((done) => {
		Bicycle.deleteMany({}, ( err, success ) => {
			if (err) console.log(err);
			done();
		});
	});

	describe('Bicicleta.createInstance', () => {
		it('crea una instancia de bicicletas', () => {
			let bike = Bicycle.createInstance(1,"verde","urbana",[-34.5,-54.1]);
			expect(bike.code).toBe(1);
			expect(bike.color).toBe("verde");
			expect(bike.model).toBe("urbana");
			expect(bike.location[0]).toEqual(-34.5);
			expect(bike.location[1]).toEqual(-54.1);
		});
	});

	describe('Bicicleta.allBici', () => {
		it('Comienza vacia', (done) => {
			Bicycle.allBicycles(( err, bike ) => {
				expect(bike.length).toBe(0);
				done();
			});
		}, 10000);
	});

	describe('Bicicleta.add', () => {
		it('Agrega una sola bicicleta', (done) => {
			let aBike = new Bicycle({code:1, color:"verde", model:"urbana"});
			Bicycle.add(aBike, ( err, newBike) => {
				if (err) console.log(err);
				Bicycle.allBicycles(( err, bicycles ) => { 
					expect(bicycles.length).toEqual(1);
					expect(bicycles[0].code).toEqual(aBike.code);
					done();
				});
			});
		});
	});

	describe('Bicicleta.findByCode', () => {
		it('debe devolver la bici con code 1', (done) => {
		let aBike = new Bicycle({code:1, color:"verde", model:"urbana"});
		Bicycle.add(aBike, ( err, newBike) => {
			if (err) console.log(err);
				Bicycle.findByCode(1, ( err, targetBici ) => { 
					expect(targetBici.code).toEqual(aBike.code);
					expect(targetBici.color).toEqual(aBike.color);
					expect(targetBici.model).toEqual(aBike.model);
					done();
				});
			});
		});
	});

	describe('Bicicleta.removeByCode', () => {
		it('debe eliminar la bicicleta con code 1', (done) => {
		let aBike = new Bicycle({code:1, color:"verde", modelo:"urbana"});
		Bicycle.add(aBike, ( err, newBike) => {
			if (err) console.log(err);
				Bicycle.removeByCode(1,( err, deleteBici ) => { 
					expect(deleteBici.code);
					done();
				});
			});
		});
	});
});