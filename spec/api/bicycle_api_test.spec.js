let mongoose = require('mongoose');
let Bicycle = require('../../models/bicycle');
let request = require('request');
let server = require('../../bin/www');

let baseUrl = "http://localhost:3000/api/bicycles";

describe('Bicycle API', () => {
  beforeEach((done) => {
    mongoose.disconnect();
		let mongoDB = "mongodb://localhost/testdb";
		mongoose.connect(mongoDB, { useNewUrlParser:true, useUnifiedTopology: true  });

		const db = mongoose.connection;
		db.on('error', console.error.bind(console, 'connection error'));
		db.once('open', () => {
			console.log('We are connected to test database');
			done();
		});
  })

  afterEach((done) => {
		Bicycle.deleteMany({}, ( err, success ) => {
			if (err) console.log(err);
			mongoose.connection.close(done);
		});
	});

  describe('GET Bicycles /', () => {
    it('Status 200', (done) => {
      request.get(baseUrl, (err, response, body) => {
        const result = JSON.parse(body);
        expect(response.statusCode).toBe(200);
        expect(result.bicycles.length).toBe(0);
        done();
      })      
    })
  })

  describe('POST Bicycles /create', () => {
    it('Status 200', (done) => {
      let headers = {'Content-Type' : 'application/json'}
      let aBicycle = '{ "code": 1, "color": "Amarillo", "model": "Urbana", "lat": -33.4666, "lng": -70.6111}';

      request.post({
        headers: headers,
        url: `${baseUrl}/create`,
        body: aBicycle
        }, (error, response, body) => {
          expect(response.statusCode).toBe(200);
          const result = JSON.parse(body).bicycle;
          expect(result.code).toBe(1);
          expect(result.color).toBe("Amarillo");
          expect(result.model).toBe("Urbana");
          expect(result.location[0]).toBe(-33.4666);
          expect(result.location[1]).toBe(-70.6111);
          done();
      })
    })
  })

  describe('PUT Bicycles /update', () => {
    it('Status 200', (done) => {
      let headers = {'Content-Type' : 'application/json'}
      let aBicycle = '{ "code": 1, "color": "Amarillo", "model": "Urbana", "lat": -33.4666, "lng": -70.6111}';

      request.post({
        headers: headers,
        url: `${baseUrl}/create`,
        body: aBicycle
        }, (error, response, body) => {
          expect(response.statusCode).toBe(200);    
      })

      let updateBicycle = '{ "code": 1, "color": "Verde", "model": "Ruta", "lat": -33.4655, "lng": -70.6122}';
      request.put({
        headers: headers,
        url: `${baseUrl}/:code/update`,
        body: updateBicycle
      }, (error, response, body) => {
        expect(response.statusCode).toBe(200);
        const result = JSON.parse(body).bicycle;
        expect(result.code).toBe(1);
        expect(result.color).toBe("Verde");
        expect(result.model).toBe("Ruta");
        expect(result.location[0]).toBe(-33.4655);
        expect(result.location[1]).toBe(-70.6122);
        done();
      })
    })
  })

  describe('DELETE Bicycles /delete', () => {
    it('Status 204', (done) => {
      let headers = {'Content-Type' : 'application/json'}
      let aBicycle = '{ "code": 1, "color": "Amarillo", "model": "Urbana", "lat": -33.4666, "lng": -70.6111}';

      request.post({
        headers: headers,
        url: `${baseUrl}/create`,
        body: aBicycle
        }, (error, response, body) => {
          expect(response.statusCode).toBe(200);    
      })

      request.delete({
        headers: headers,
        url: `${baseUrl}/delete`,
      }, (error, response, body) => {
        expect(response.statusCode).toBe(204);
        expect(Bicycle.removeByCode(1));
        done();
      })
    })
  })
})
