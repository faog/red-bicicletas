let Bicycle = require('../models/bicycle');

exports.bicyclesList =  function (req,res) {
  Bicycle.getAll(function (error, result) {
      res.render('bicycles/index', {bicycles: result});
  });
}

exports.bicycleCreateGet = (req, res) => {
  res.render('bicycles/create');
}

exports.bicycleCreatePost = (req, res) => {
  let bicycle = new Bicycle({
    code: req.body.id, 
    color: req.body.color, 
    model: req.body.model,
    location: [req.body.lat, req.body.lng]
  });
  Bicycle.add(bicycle, function (error, newElement) { 
      res.redirect('/bicycles');
  });
}

exports.bicycleUpdateGet = async (req, res) => {
  let bicycle = await Bicycle.findByCode(req.params.code);
  res.render('bicycles/update', {bicycle});
}

exports.bicycleUpdatePost = (req, res) => {
  Bicycle.findByCode(req.params.code, function (err, bicycle) {
    bicycle.code = req.body.code;
    bicycle.color = req.body.color;
    bicycle.model = req.body.model;
    bicycle.location = [req.body.lat, req.body.lng];
    bicycle.save();
    
    res.redirect('/bicycles');
  });
}

exports.bicycleDeletePost = async (req, res) => {
  await Bicycle.removeByCode(req.body.code);
  res.redirect('/bicycles')
}
