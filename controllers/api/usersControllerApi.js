const User = require("../../models/user");

exports.usersList = async(req, res) => {
  const users = await User.find();
  res.status(200).json({ 
    users
  })
}

exports.userCreate = async (req, res) => {
  let user = new User({
    name: req.body.name,
    email: req.body.email,
    password: req.body.password
  })

  await user.save((err) => {
    if (err) return res.status(500).json(err)
    res.status(200).json({ user })
  })
}

exports.userReservation = async (req, res) => {
  const { _id, bikeId, startDate, endDate } = req.body;
  User.findById(req.body._id, (error, usr) => {
    usr.reserve(bikeId, startDate, endDate, () => {
      res.status(200).json({ reservation : {bikeId, startDate, endDate}})
    })
  })
}