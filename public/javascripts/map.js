let map = L.map('map-container').setView([-33.4372, -70.6506], 15);

L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
  attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
}).addTo(map);

$.ajax({
  dataType: "json",
  url:"api/bicycles",
  success: (result) => {
    console.log(result);
    result.bicycles.forEach((bicycle)=>{
      L.marker(bicycle.location, {title: bicycle.id}).addTo(map);
    })
  }
})