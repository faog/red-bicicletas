let express = require('express');
let router = express.Router();
let bicycleController = require('../../controllers/api/bicycleControllerApi');

router.get('/', bicycleController.bicyclesList);
router.post('/create', bicycleController.bicycleCreate);
router.delete('/delete', bicycleController.bicycleDelete);
router.put('/:code/update', bicycleController.bicycleUpdate);

module.exports = router;