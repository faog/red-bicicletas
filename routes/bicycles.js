var express = require('express');
var router = express.Router();
var bicycleController = require('../controllers/bicycle');

router.get('/', bicycleController.bicyclesList);
router.get('/create', bicycleController.bicycleCreateGet);
router.post('/create', bicycleController.bicycleCreatePost);
router.get('/:code/update', bicycleController.bicycleUpdateGet);
router.post('/:code/update', bicycleController.bicycleUpdatePost);
router.post('/:code/delete', bicycleController.bicycleDeletePost);

module.exports = router;

