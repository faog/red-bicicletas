const nodemailer = require ('nodemailer');
const sgTransport = require('nodemailer-sendgrid-transport');

let mailConfig;

const getMailTransport = (environment) => {
  if (environment === 'production'){
    const options = {
      auth: {
        api_key: process.env.SENDGRID_API_SECRET
      }
    }
    return nodemailer.createTransport(sgTransport(options))
  }
  if (environment === 'staging'){
    const options = {
      auth: {
        api_key: process.env.SENDGRID_API_SECRET
      }
    }
    return nodemailer.createTransport(sgTransport(options))
  }
  // SMTP Default
  mailConfig = {
    host:'smtp.ethereal.email',
    port: 587,
    auth: {
      user: process.env.ethereal_user,
      pass: process.env.ethereal_pwd
    }
  }
  return nodemailer.createTransport(mailConfig);
} 

module.exports = getMailTransport(process.env.NODE_ENV);
