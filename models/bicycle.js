let mongoose = require('mongoose');
let Schema = mongoose.Schema;

let bicycleSchema = new Schema({
  code : Number,
  color : String,
  model : String,
  location: {
    type: [Number], index: { type: '2dsphere', sparse: true }
  }
});

bicycleSchema.statics.createInstance = function(code, color, model, location){
  return new this({
    code : code,
    color : color,
    model : model,
    location : location
  });
};

bicycleSchema.statics.updateInstance = function (code, newcode, color, model, location) {  
  this.findOne({ code: code }, function (err, update){
    update.code = newcode;
    update.code = newcode;
    update.color = color;
    update.model = model;
    update.location = location;
    update.save();
  });
};

bicycleSchema.methods.toString = function() {
  return 'Code: ' + this.code + ' Color: ' + this.color;
};

bicycleSchema.statics.getAll = function (cb) {  
  return this.find({}, cb);
};

bicycleSchema.statics.add = function(aBike, cb){
  this.create(aBike,cb);
};

bicycleSchema.statics.findByCode = function(code, cb){
  return this.findOne({ code:code }, cb);
};

bicycleSchema.statics.removeByCode = function(code, cb){
  return this.deleteOne({ code:code }, cb);
};

module.exports = mongoose.model('Bicycle', bicycleSchema);