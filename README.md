## Red de Bicicletas

### I. Instalación

```
npm install
```

### II. Ejecución servidor

```
npm run devstart
```
### III. Credenciales usuario Admin

- ambiente development y production

```js
  email: admin@redbicicletas.com
  password: admin
```
### IV. Deploy red bicicletas 

https://red-bicycle.herokuapp.com/

### V. Colección enpoints

**1. Bicicletas**

a. GET Bicycles

```js
http://localhost:3000/api/bicycles
```

```js
{
  "bicycles": [{
    "location": [
      Number,
      Number
    ],
    "_id": Number,
    "color": String,
    "model": String
  }]
}
```

b. POST Bicycles

```js
http://localhost:3000/api/bicycles/create
```
```js
{
  "id": Number,
  "color": String,
  "model": String,
  "lat": Number,
  "lng": Number
}
```

c. UPDATE Bicycles (Put)

```js
http://localhost:3000/api/bicycles/:code/update
```

```js
http://localhost:3000/api/bicycles/1/update

{
  "color": String,
  "model": String,
  "lat": Number,
  "lng": Number
}
```

d. DELETE Bicycles

```js
http://localhost:3000/api/bicycles/delete
```

```js
{
  "id": Number
}
```

**2. Usuarios**

a. GET Bicycles

```js
http://localhost:3000/api/users
```

```js
{
  "user": {
    "_id": String,
    "name": String
  }
}
```

b. POST Bicycles

```js
http://localhost:3000/api/users/create
```
```js
{
  "name": String
}
```

**3. Reservación**

a. POST Reservation

```js
http://localhost:3000/api/users/reserve
```
```js
{
  "_id": String,
  "bikeId": String,
  "startDate": Date,
  "endDate": Date
}
```

**4. Autenticación**

a. POST authenticate

```js
http://localhost:3000/api/auth/authenticate
```
```js
{
  "email": String,
  "password": String
}
```

### Ejecución unit test

```
npm run test
```

### Autor: Fabiola Orellana